'''system module.'''
import json
from urllib.error import URLError
import requests
from bs4 import BeautifulSoup
URL = 'https://www.Udacity.com/school-of-ai'

class UdacityScapper():
    '''a class docstring'''
    @staticmethod
    def udacity_courses():
        ''' this function fetch all available courses of ai on site '''
        url = 'https://www.Udacity.com/school-of-ai'
        try:
            response = requests.get(url)
        except URLError as url_error:
            raise Exception("Server Not Found, please check the url")
        # request for all ai courses
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        # fetching list of courses
        try:
            article = soup.find('ul' , {'data-testid': 'upcoming-programs-list'})
        except AttributeError:
            raise Exception("not found ul tag")
        # getting response from server
        response = requests.get(url)
        udacity = []
        for property in article.find_all('li'):
            title = property.find('h3' , class_='upcoming-programs-list_title__2v_yd').text
            concepts_covered = property.find('p' , class_= 'upcoming-programs-list_conceptsCovered__3LZ01').text
            enrolled_tag =  property.find('time' , class_= 'upcoming-programs-list_enrollBy__FDLQy')
            left_to_enroll = enrolled_tag.text if enrolled_tag else ""
            property_info = {
            'title': title,
            'skills' : concepts_covered,
            'left_to_enroll' : left_to_enroll
            }
            udacity.append(property_info)
        return json.dumps(udacity)
